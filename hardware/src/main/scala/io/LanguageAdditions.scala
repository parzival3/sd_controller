package io

import Chisel._

object Ranges {

    def range(address: UInt)(blocks: (UInt => Unit)*) {
        blocks.foreach{block => block(address)}
    }

    def interval(base: Int, offset: Int)(action: => Unit): UInt => Unit =
        comp => when(comp >= base.U && comp <= (base+offset).U)(action)

    def precisely(value: Int)(action: => Unit): UInt => Unit =
        comp => when(comp === value.U)(action)
}