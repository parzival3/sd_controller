package io

import Chisel._
import ocp._

import Ranges._

object SDHostController extends DeviceObject {
  def init(params: Map[String, String]) = {

  }

  def create(params: Map[String, String]) = {
    Module(new SDHostController())
  }

  trait Pins {
    val sDHostControllerPins = new Bundle() {
      val clk = Output(Bits(width = 1))
      val cmdIn = Input(Bits(width = 1))
      val cmdOut = Output(Bits(width = 1))
      val cmdToggle = Output(Bits(width = 1))
      val dataIn = Input(Bits(width = 1))
      val dataOut = Output(Bits(width = 1))
      val dataToggle = Output(Bits(width = 1))
    }
  }
}

object CmdState {
  val idle = 0.U
  val send = 1.U
  val waitResponse = 2.U
  val receive = 3.U
}

object DataState {
  val idle = 0.U
  val waitSend = 1.U
  val send = 2.U
  val waitReceive = 3.U
  val receive = 4.U
}

object Commands {
  val READ_BLOCK = 17
  val WRITE_BLOCK = 24
}

object Constants {
  val RESPONSE_REG_WIDTH = 17
}

object StatusFlags{
  val CMD_SENDING = 0
  val CARD_AVAILABLE = 1
  val ERROR = 2
  val DATA_READY = 3
  val RESPONSE_READY = 4
}

object RegisterAddresses {
  val CMD_REG_LOW  = 0x0000
  val CMD_REG_HIGH = 0x0004
  val RESPONSE_REG = 0x0008
  val STATUS_REG = 0x001c
  val CLOCK_DIV = 0x0020
  val DATA_BUF_START = 0x8000
}

class DividedClock() extends Module {
  val io = IO(new Bundle{
    val risingEdge = Output(Bits(width = 1))
    val fallingEdge = Output(Bits(width = 1))
    val clk = Output(Bits(width = 1))
    val countsPerHalfCycle = Input(UInt(width = 16))
  })

  val cnt = Reg(UInt(width = 32))

  val clkReg = RegInit(Bits(0, width = 1))
  io.clk := clkReg
  io.fallingEdge := false.B
  io.risingEdge := false.B

  when (cnt > 1.U) {
    cnt := cnt - 1.U
  } otherwise {
    io.fallingEdge := clkReg === true.B
    io.risingEdge := clkReg === false.B
    clkReg := ~clkReg
    cnt := io.countsPerHalfCycle
  }
}

object Helpers {
  def assignSubset[T <: Bits](element: T, value: T)(lo: UInt): Unit =
    for (i <- 31 to 0 by -1) element(i.U+lo) := value(i)
}

class Timeout(freq: Int, mu_s: Int) extends Module {
  val io = IO(new Bundle{
    val reset = Input(Bool())
    val triggered = Output(Bool())
    //val sdClk = Input(UInt(width=1))
  })

  val totalTicks = freq/1000000*mu_s


  val counter = RegInit(Bits(totalTicks, width = log2Up(totalTicks)))

  io.triggered := false.B

  when (io.reset) {
    counter := totalTicks.U
    io.triggered := false.B
  }.elsewhen(counter > 0.U) {
    counter := counter - 1.U
  }.otherwise{
    io.triggered := true.B
  }
}

class SDHostController(freq: Int = 50000000, dataBufWidth: Int = 128) extends CoreDevice {
  override val io = new CoreDeviceIO() with SDHostController.Pins

  // Addressable registers
  val clockDivReg = RegInit(Bits(5, width = 32))
  val cmdReg = Reg(Bits(width = 48))
  val responseReg = Reg(Bits(width = 136))
  val statusReg = RegInit(Bits(0, width = 32))
  val dataBuf = Vec.fill(dataBufWidth)(RegInit(Bits(0, width=32)))
  val dataReg = Reg(init = UInt(0, 32))
  val respReg = Reg(init = OcpResp.NULL)

  io.sDHostControllerPins.cmdToggle := 1.U
  io.sDHostControllerPins.cmdOut := 1.U

  // Clock setup
  val sdClk = Wire(Bits(width = 1))
  val fallingEdge = Wire(Bits(width = 1))
  io.sDHostControllerPins.clk := sdClk

  val clkManager = Module(new DividedClock())
  sdClk := clkManager.io.clk
  fallingEdge := clkManager.io.risingEdge
  clkManager.io.countsPerHalfCycle := clockDivReg

  val cmdState = Reg(init=CmdState.idle)

  // Data registers
  val dataState = Reg(init=DataState.idle)
  val dataBufIndex = RegInit(Bits(0, width=log2Ceil(dataBufWidth)+1))
  val dataBufBitIndex = RegInit(Bits(0, width=7))
  val dataWaitCnt = RegInit(Bits(2, width=2))
  val dataTmp = RegInit(Bits(0, width=32))

  // Controller component generation
  genOCPControl()
  genCMDControl()
  genDataControl()

  def genOCPControl() {
    switch(io.ocp.M.Cmd) {

      is(OcpCmd.WR) {
        val deviceAddr = io.ocp.M.Addr(15, 0)
        val bufRelativeAddr = deviceAddr(14, 0)

        range(io.ocp.M.Addr(15, 0)) (
          precisely(RegisterAddresses.CMD_REG_LOW){
            cmdReg(31, 0) := io.ocp.M.Data
          },

          precisely(RegisterAddresses.CMD_REG_HIGH){
            cmdReg(47, 32) := io.ocp.M.Data(15, 0)
            statusReg(StatusFlags.ERROR) := 0.U // Reset error
            statusReg(StatusFlags.CMD_SENDING) := 1.U
          },

          precisely(RegisterAddresses.CLOCK_DIV) {
            clockDivReg := io.ocp.M.Data
          },

          interval(RegisterAddresses.DATA_BUF_START, dataBufWidth-4){
            dataBuf(bufRelativeAddr) := io.ocp.M.Data
          }
        )
      }

      is(OcpCmd.RD) {
        respReg := OcpResp.DVA
        val deviceAddr = io.ocp.M.Addr(15, 0)
        val bufRelativeAddr = deviceAddr(14, 0)
        range(io.ocp.M.Addr(15, 0)) (
          interval(RegisterAddresses.RESPONSE_REG, Constants.RESPONSE_REG_WIDTH-4) {
            val offset = (deviceAddr - RegisterAddresses.RESPONSE_REG.U)*8.U

            dataReg := responseReg(offset+31.U, offset)
          },

          precisely(RegisterAddresses.STATUS_REG) {
            dataReg := statusReg
          },

          precisely(RegisterAddresses.CLOCK_DIV) {
            dataReg := clockDivReg
          },

          interval(RegisterAddresses.DATA_BUF_START, dataBufWidth-4) {
            dataReg := dataBuf(bufRelativeAddr)
          }
        )
      }
      when(io.ocp.M.Cmd === OcpCmd.RD || io.ocp.M.Cmd === OcpCmd.WR) {
        respReg := OcpResp.DVA
      }.otherwise {
        respReg := OcpResp.NULL
      }
    }
  }

  def genCMDControl() {

    val commandSendCnt = Reg(Bits(width=6))
    val receiveWaitCnt = RegInit(Bits(2, width=2))

    val responseLength = Reg(Bits(width=8))

    val timeOut = Module(new Timeout(freq, 250000))
    timeOut.io.reset := true.B

    when(cmdState === CmdState.waitResponse || cmdState === CmdState.receive) {
      timeOut.io.reset := false.B
    }.otherwise{
      timeOut.io.reset := true.B
    }

    switch(cmdState) {
      is(CmdState.idle) {
        io.sDHostControllerPins.cmdToggle := 0.U
        io.sDHostControllerPins.cmdOut := 1.U
        when(fallingEdge === true.B && statusReg(StatusFlags.CMD_SENDING)) {
          commandSendCnt := 0.U

          // Set response length
          val cmdIndex = cmdReg(45, 40)

          when (cmdIndex === Bits("b0010") ||
                cmdIndex === Bits("b1001") ||
                cmdIndex === Bits("b1010")) {
                  responseLength := 134.U
          }.otherwise {
            responseLength := 46.U
          }
          statusReg(StatusFlags.ERROR) := 0.U
          cmdState := CmdState.send
        }
      }

      is(CmdState.send) {
        io.sDHostControllerPins.cmdOut := cmdReg(commandSendCnt)
        when(fallingEdge === true.B) {
          when(commandSendCnt < 47.U) {
            commandSendCnt := commandSendCnt + 1.U
          }.otherwise{
            val cmdIndex = cmdReg(45, 40)
            when(cmdIndex === Bits("b0000")) {
              cmdState := CmdState.idle
              statusReg(StatusFlags.CMD_SENDING) := 0.U
            }.otherwise {
              cmdState := CmdState.waitResponse
              timeOut.io.reset := true.B
            }
          }
        }
      }

      is(CmdState.waitResponse) {
        io.sDHostControllerPins.cmdToggle := 0.U
        responseReg := 0.U
        when(timeOut.io.triggered) {
          cmdState := CmdState.idle
          statusReg(StatusFlags.ERROR) := 1.U
          statusReg(StatusFlags.CMD_SENDING) := 0.U
        }.elsewhen(fallingEdge === true.B) {
          when(receiveWaitCnt === 0.U && io.sDHostControllerPins.cmdIn === 0.U) {
            cmdState := CmdState.receive
            receiveWaitCnt := 2.U
          }.elsewhen(receiveWaitCnt > 0.U) {
            receiveWaitCnt := receiveWaitCnt - 1.U
          }
        }
      }

      is(CmdState.receive) {
        io.sDHostControllerPins.cmdToggle := 0.U
        responseReg(responseLength) := io.sDHostControllerPins.cmdIn
        when(timeOut.io.triggered) {
          cmdState := CmdState.idle
          statusReg(StatusFlags.ERROR) := 1.U
          statusReg(StatusFlags.CMD_SENDING) := 0.U
        }.elsewhen(fallingEdge === true.B && responseLength > 0.U) {
          responseLength := responseLength - 1.U
        }.elsewhen(responseLength === 0.U) {
          statusReg(StatusFlags.RESPONSE_READY) := 1.U
          statusReg(StatusFlags.CMD_SENDING) := 0.U
          cmdState := CmdState.idle
        }
      }
    }
  }

  def genDataControl() {


    io.sDHostControllerPins.dataToggle :=
      Mux(dataState === DataState.waitReceive || dataState === DataState.receive, 1.U, 0.U)

    switch (dataState) {
      is (DataState.idle) {
        when (fallingEdge === true.B && cmdState === CmdState.waitResponse) {
          val cmdIndex = cmdReg(45, 40)

          dataBufIndex := (dataBufWidth-1).U
          dataBufBitIndex := 31.U

          when (cmdIndex === Commands.READ_BLOCK.U) {
            dataState := DataState.waitReceive
          }.elsewhen(cmdIndex === Commands.WRITE_BLOCK.U) {
            dataState := DataState.waitSend
          }
        }
      }

      is (DataState.waitReceive) {
        when(fallingEdge === true.B) {
          when(dataWaitCnt > 0.U) {
            dataWaitCnt := dataWaitCnt - 1.U
          }

          when(dataWaitCnt === 0.U && io.sDHostControllerPins.dataIn === 0.U) {
            dataState := DataState.receive
          }
        }
      }

      is (DataState.receive) {
        when(fallingEdge === true.B) {
          dataTmp(dataBufBitIndex) := io.sDHostControllerPins.dataIn
          dataBufBitIndex := dataBufBitIndex - 1.U
          when(dataBufBitIndex === 0.U) {
            dataBufBitIndex := 31.U
            dataBuf(dataBufIndex) := dataTmp
            dataBufIndex := dataBufIndex - 1.U
            dataTmp := 0.U

            when(dataBufIndex === 0.U) {
              statusReg(StatusFlags.DATA_READY) := 1.U
              dataState := DataState.idle
            }
          }


          //dataBuf(dataBufIndex)(dataBufBitIndex) := io.sDHostControllerPins.dataIn
        }
      }

      is (DataState.waitSend) {

      }

      is (DataState.send) {

      }
    }
  }
  io.ocp.S.Data := dataReg
  io.ocp.S.Resp := respReg
}
