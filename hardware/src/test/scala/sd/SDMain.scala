package sd

import Chisel.{Module, chiselMainTest}
import io.SDHostController
import sys.process._

object SDMain {
  def main(args: Array[String]): Unit = {
    chiselMainTest(Array("--genHarness", "--test", "--backend", "c",
      "--compile", "--vcd", "--targetDir", "generated/"+this.getClass.getSimpleName.dropRight(1)),
      () => Module(new SDHostController(50000000, 4))) {
      dut => new SDPlayground(dut)
    }
    "gtkwave generated/"++this.getClass.getSimpleName.dropRight(1)+"/"+"SDHostController.vcd" !
  }
}

object SDAutomated {
  def main(args: Array[String]): Unit = {
    chiselMainTest(Array("--genHarness", "--test", "--backend", "c",
    "--compile"),
      () => Module(new SDHostController(50000000, 4))) {
        dut => new SDAutomaticTests(dut)
      }
    }
}