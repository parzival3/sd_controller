package sd

import Chisel._
import ocp._
import io.SDHostController
import io.RegisterAddresses
import io.StatusFlags
import io.Commands
import io.DataState
import io.CmdState

object Responses {
  val R1_STD = List(0, 0, 1, 0, 1, 0, 1, 0,
                    1, 0, 1, 0, 1, 0, 1, 0,
                    1, 0, 1, 0, 1, 0, 1, 0,
                    1, 0, 1, 0, 1, 0, 1, 0,
                    1, 0, 1, 0, 1, 0, 1, 0,
                    1, 0, 1, 0, 1, 0, 1, 1)
  val R1_STD_HEX = 0x2AAAAAAAAAABL
  val DATA_STD = List(1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
                      1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
                      1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
                      1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0)
}

object StatusFlagsNumerical {
  val CMD_SENDING = 1
  val CARD_AVAILABLE = 2
  val ERROR = 4
  val DATA_READY = 8
  val RESPONSE_READY = 16
}

object Constants {
  val CLOCK_DIV = 5
  val DEVICE_ADDRESS = 0xf00b0000
}

abstract class SDUnitTest(c: SDHostController) extends Tester(c) {
  def write(addr: Int, data: Int) {
    poke(c.io.ocp.M.Cmd, 0x001)
    poke(c.io.ocp.M.Addr, addr+Constants.DEVICE_ADDRESS)
    poke(c.io.ocp.M.Data, data)
    step(Constants.CLOCK_DIV)
    poke(c.io.ocp.M.Cmd, 0)
  }

  def read(addr: Int): BigInt = {
    poke(c.io.ocp.M.Cmd, 0x002)
    poke(c.io.ocp.M.Addr, addr+Constants.DEVICE_ADDRESS)
    step(1)
    stepUntil(Constants.CLOCK_DIV)(peek(c.io.ocp.S.Resp) == 1)
    val res = peek(c.io.ocp.S.Data)
    step(1)
    poke(c.io.ocp.M.Cmd, 0)

    res
  }

  def readExpect(addr: Int, expected: BigInt): Unit = {
    poke(c.io.ocp.M.Cmd, 0x002)
    poke(c.io.ocp.M.Addr, addr+Constants.DEVICE_ADDRESS)
    step(1)
    stepUntil(Constants.CLOCK_DIV)(peek(c.io.ocp.S.Resp) == 1)
    expect(c.io.ocp.S.Data, expected)
    step(1)
    poke(c.io.ocp.M.Cmd, 0)
  }

  def binary(v: String): Long = {
    java.lang.Long.parseLong(v, 2)
  }

  def createCmd(cmdIndex: Long, args: Long): Long = {
    binary("010000000000000000000000000000000000000000000001") | (cmdIndex << 40) | (args << 7)
  }

  def stepUntil(s: Int)(condition: Boolean) {
    while(!condition) step(s)
  }

  /** Step the number of processor cycles neccesarry for `cycles` number of controller clock cycles
   *
   * @param cycles numer of sd clock cycles
   */
  def stepSDClk(cycles: Int) {
    step(cycles*2*Constants.CLOCK_DIV)
  }

  def waitForCommandStartedSending() {
    stepUntil(2*Constants.CLOCK_DIV)((peek(c.statusReg) & 0x1) == 1)
  }

  def waitForCommandFinishedSending() {
    waitForCommandStartedSending
    step((1+48)*2*Constants.CLOCK_DIV)
  }

  /** Emulates a single bit sent from the card to the controller on the CMD line.
   *
   * @param bit the value to send
   */
  def responseBit(bit: Int) {
    poke(c.io.sDHostControllerPins.cmdIn, bit)
    step(2*Constants.CLOCK_DIV)
  }

  /** Emulates a single bit sent from the card to the controller on the CMD line.
   *
   * @param bit the value to send
   */
  def dataBit(bit: Int) {
    poke(c.io.sDHostControllerPins.dataIn, bit)
    step(2*Constants.CLOCK_DIV)
  }

  /** Emulates the command response from a card
   *
   * @param value a list of bitvalues to send
   */
  def cardSend(sendFunc: Int => Unit)(value: List[Int]) {
    for (v <- value) {
      sendFunc(v)
    }
  }

  def testSendReadCommand() {
    val readCmd = createCmd(Commands.READ_BLOCK, 0)

    write(RegisterAddresses.CMD_REG_LOW, readCmd)
    write(RegisterAddresses.CMD_REG_HIGH, readCmd >> 32)

    waitForCommandStartedSending

    stepSDClk(47)

    //expect(c.dataState, DataState.receive.litValue())

    poke(c.io.sDHostControllerPins.dataIn, 1)

    stepSDClk(2)

    poke(c.io.sDHostControllerPins.cmdIn, 1)
    cardSend(responseBit)(Responses.R1_STD)

    poke(c.io.sDHostControllerPins.dataIn, 0)

    cardSend(dataBit)(Responses.DATA_STD)

    stepSDClk(50)

    expect(c.statusReg, StatusFlagsNumerical.RESPONSE_READY | StatusFlagsNumerical.DATA_READY)
  }

  def testSendCommand() {
    //expect(c.statusReg, 0)

    write(addr=RegisterAddresses.CMD_REG_LOW, data=0x12345678)
    write(addr=RegisterAddresses.CMD_REG_HIGH, data=0x9abcdef0)

    //expect(c.statusReg, 1)

    waitForCommandFinishedSending


    poke(c.io.sDHostControllerPins.cmdIn, 1)
    step(8*Constants.CLOCK_DIV)

    cardSend(responseBit)(Responses.R1_STD)

    //expect(c.statusReg, 16)

    val resp = read(addr=RegisterAddresses.RESPONSE_REG)
    print(resp)

    step(50)
  }

  def resetController() {
    poke(c.cmdReg, 0)
    poke(c.statusReg, 0)
    poke(c.responseReg, 0)
    for (bufReg <- c.dataBuf) {
      poke(bufReg, 0)
    }
  }

  resetController()
  println(this.getClass.getName )
}

class SDPlayground(c: SDHostController) extends SDUnitTest(c) {
  testSendReadCommand()
}

class SDSendCommandTest(c: SDHostController) extends SDUnitTest(c) {
  expect(c.statusReg, 0)

  write(addr=RegisterAddresses.CMD_REG_LOW, data=0x12345678)
  write(addr=RegisterAddresses.CMD_REG_HIGH, data=0x9abcdef0)

  expect(c.statusReg, StatusFlags.CMD_SENDING)

  waitForCommandFinishedSending


  poke(c.io.sDHostControllerPins.cmdIn, 1)
  step(2*Constants.CLOCK_DIV)

  cardSend(responseBit)(Responses.R1_STD)

  expect(c.statusReg, StatusFlags.RESPONSE_READY)

  readExpect(addr=RegisterAddresses.RESPONSE_REG, Responses.R1_STD_HEX)

  step(50)
}

class SDReadStatusRegisterTest(c: SDHostController) extends SDUnitTest(c) {
  expect(c.statusReg, 0)

  val statusValue = StatusFlagsNumerical.CARD_AVAILABLE | StatusFlagsNumerical.ERROR
  poke(c.statusReg, statusValue)

  // Test that the correct value is returned
  readExpect(RegisterAddresses.STATUS_REG, statusValue)
}

class SDReadCommandTest(c: SDHostController) extends SDUnitTest(c) {
  val readCmd = createCmd(Commands.READ_BLOCK, 0)
  // print("hej")
  // println(readCmd)
  write(RegisterAddresses.CMD_REG_LOW, readCmd)
  write(RegisterAddresses.CMD_REG_HIGH, readCmd >> 32)

  waitForCommandStartedSending

  stepSDClk(47)

  expect(c.dataState, DataState.receive.litValue())

  poke(c.io.sDHostControllerPins.cmdIn, 1)
  poke(c.io.sDHostControllerPins.dataIn, 1)

  stepSDClk(2)
  cardSend(responseBit)(Responses.R1_STD)

  poke(c.io.sDHostControllerPins.dataIn, 0)

  cardSend(dataBit)(Responses.DATA_STD)

  stepSDClk(50)

  expect(c.statusReg, StatusFlagsNumerical.RESPONSE_READY | StatusFlagsNumerical.DATA_READY)

}

class SDAutomaticTests(c: SDHostController) extends Tester(c) {
  new SDSendCommandTest(c)
  new SDReadStatusRegisterTest(c)
  new SDReadCommandTest(c)
}