--
-- Copyright: 2013, Technical University of Denmark, DTU Compute
-- Author: Martin Schoeberl (martin@jopdesign.com)
--         Rasmus Bo Soerensen (rasmus@rbscloud.dk)
-- License: Simplified BSD License
--

-- VHDL top level for Patmos in Chisel on Altera de2-115 board
--
-- Includes some 'magic' VHDL code to generate a reset after FPGA configuration.
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity patmos_top is
	port(
		clk : in  std_logic;
		oLedsPins_led : out std_logic_vector(8 downto 0);
		iKeysPins_key : in std_logic_vector(3 downto 0);
		oUartPins_txd : out std_logic;
		iUartPins_rxd : in  std_logic;
        oSRAM_A : out std_logic_vector(19 downto 0);
		SRAM_DQ : inout std_logic_vector(15 downto 0);
		SD_CLK : inout std_logic;
		SD_CMD : inout std_logic;
		SD_DAT0 : inout std_logic;
    SD_CLK_C : out std_logic;
		SD_CMD_C : out std_logic;
		SD_DAT0_C : out std_logic;
		SD_CMD_TOGGLE_C : out std_logic;
		SD_DATA_TOGGLE_C : out std_logic;
    oSRAM_CE_N : out std_logic;
    oSRAM_OE_N : out std_logic;
    oSRAM_WE_N : out std_logic;
    oSRAM_LB_N : out std_logic;
    oSRAM_UB_N : out std_logic
	);
end entity patmos_top;

architecture rtl of patmos_top is
	component Patmos is
		port(
			clk             : in  std_logic;
			reset           : in  std_logic;

			io_comConf_M_Cmd        : out std_logic_vector(2 downto 0);
			io_comConf_M_Addr       : out std_logic_vector(31 downto 0);
			io_comConf_M_Data       : out std_logic_vector(31 downto 0);
			io_comConf_M_ByteEn     : out std_logic_vector(3 downto 0);
			io_comConf_M_RespAccept : out std_logic;
			io_comConf_S_Resp       : in std_logic_vector(1 downto 0);
			io_comConf_S_Data       : in std_logic_vector(31 downto 0);
			io_comConf_S_CmdAccept  : in std_logic;

			io_comSpm_M_Cmd         : out std_logic_vector(2 downto 0);
			io_comSpm_M_Addr        : out std_logic_vector(31 downto 0);
			io_comSpm_M_Data        : out std_logic_vector(31 downto 0);
			io_comSpm_M_ByteEn      : out std_logic_vector(3 downto 0);
			io_comSpm_S_Resp        : in std_logic_vector(1 downto 0);
			io_comSpm_S_Data        : in std_logic_vector(31 downto 0);

			io_ledsPins_led : out std_logic_vector(8 downto 0);
			io_keysPins_key : in  std_logic_vector(3 downto 0);
			io_uartPins_tx  : out std_logic;
			io_uartPins_rx  : in  std_logic;

			io_sramCtrlPins_ramOut_addr : out std_logic_vector(19 downto 0);
			io_sramCtrlPins_ramOut_doutEna : out std_logic;
			io_sramCtrlPins_ramIn_din : in std_logic_vector(15 downto 0);
			io_sramCtrlPins_ramOut_dout : out std_logic_vector(15 downto 0);
			io_sramCtrlPins_ramOut_nce : out std_logic;
			io_sramCtrlPins_ramOut_noe : out std_logic;
			io_sramCtrlPins_ramOut_nwe : out std_logic;
			io_sramCtrlPins_ramOut_nlb : out std_logic;
			io_sramCtrlPins_ramOut_nub : out std_logic;

      io_sDHostControllerPins_clk : out std_logic;
			io_sDHostControllerPins_cmdIn : in std_logic;
			io_sDHostControllerPins_cmdOut : out std_logic;
			io_sDHostControllerPins_cmdToggle : out std_logic;
			io_sDHostControllerPins_dataIn : in std_logic;
			io_sDHostControllerPins_dataOut : out std_logic;
			io_sDHostControllerPins_dataToggle : out std_logic

		);
	end component;

	-- DE2-70: 50 MHz clock => 80 MHz
	-- BeMicro: 16 MHz clock => 25.6 MHz
	constant pll_infreq : real    := 50.0;
	constant pll_mult   : natural := 8;
	constant pll_div    : natural := 5;

	signal clk_int : std_logic;

	-- for generation of internal reset
	signal int_res            : std_logic;
	signal res_reg1, res_reg2 : std_logic;
	signal res_cnt            : unsigned(2 downto 0) := "000"; -- for the simulation

    -- sram signals for tristate inout
    signal sram_out_dout_ena : std_logic;
		signal sram_out_dout : std_logic_vector(15 downto 0);
	
	-- sd-card signals
	signal sdcmd_out_dout_ena : std_logic;
	signal sdcmd_out_dout : std_logic;
	signal sddata_out_dout_ena : std_logic;
	signal sddata_out_dout : std_logic;

	attribute altera_attribute : string;
	attribute altera_attribute of res_cnt : signal is "POWER_UP_LEVEL=LOW";

begin
	pll_inst : entity work.pll generic map(
			input_freq  => pll_infreq,
			multiply_by => pll_mult,
			divide_by   => pll_div
		)
		port map(
			inclk0 => clk,
			c0     => clk_int
		);
	-- we use a PLL
	-- clk_int <= clk;

	--
	--	internal reset generation
	--	should include the PLL lock signal
	--
	process(clk_int)
	begin
		if rising_edge(clk_int) then
			if (res_cnt /= "111") then
				res_cnt <= res_cnt + 1;
			end if;
			res_reg1 <= not res_cnt(0) or not res_cnt(1) or not res_cnt(2);
			res_reg2 <= res_reg1;
			int_res  <= res_reg2;
		end if;
	end process;


    -- tristate output to ssram
    process(sram_out_dout_ena, sram_out_dout)
    begin
      if sram_out_dout_ena='1' then
        SRAM_DQ <= sram_out_dout;
      else
        SRAM_DQ <= (others => 'Z');
      end if;
		end process;
		
		-- tristate output to sdcard CMD
    process(sdcmd_out_dout_ena, sdcmd_out_dout)
		begin
			--SD_CMD <= sdcmd_out_dout;
      if sdcmd_out_dout_ena='1' then
        SD_CMD <= sdcmd_out_dout;
      else
        SD_CMD <= 'Z';
      end if;
		end process;

		process(sddata_out_dout_ena, sddata_out_dout)
		begin
			SD_DATA_TOGGLE_C <= sddata_out_dout_ena;
      if sddata_out_dout_ena='1' then
        SD_DAT0 <= sddata_out_dout;
      else
        SD_DAT0 <= 'Z';
      end if;
    end process;
		 SD_CMD_TOGGLE_C <= sdcmd_out_dout_ena;
     SD_CLK_C <= SD_CLK;
     SD_CMD_C <= SD_CMD;
		 SD_DAT0_C <= SD_DAT0;
		 

  comp : Patmos port map(
			clk                                  =>   clk_int,
			reset                                =>   int_res,

			io_comConf_M_Cmd                     =>   open,
			io_comConf_M_Addr                    =>   open,
			io_comConf_M_Data                    =>   open,
			io_comConf_M_ByteEn                  =>   open,
			io_comConf_M_RespAccept              =>   open,
			io_comConf_S_Resp                    =>   (others => '0'),
			io_comConf_S_Data                    =>   (others => '0'),
			io_comConf_S_CmdAccept               =>   '0',

			io_comSpm_M_Cmd                      =>   open,
			io_comSpm_M_Addr                     =>   open,
			io_comSpm_M_Data                     =>   open,
			io_comSpm_M_ByteEn                   =>   open,
			io_comSpm_S_Resp                     =>   (others => '0'),
			io_comSpm_S_Data                     =>   (others => '0'),

			io_ledsPins_led                      =>   oLedsPins_led,
			io_keysPins_key                      =>   iKeysPins_key,
			io_uartPins_tx                       =>   oUartPins_txd,
			io_uartPins_rx                       =>   iUartPins_rxd,

			io_sramCtrlPins_ramOut_addr          =>   oSRAM_A,
			io_sramCtrlPins_ramOut_doutEna       =>   sram_out_dout_ena,
			io_sramCtrlPins_ramIn_din            =>   SRAM_DQ,
			io_sramCtrlPins_ramOut_dout          =>   sram_out_dout,
			io_sramCtrlPins_ramOut_nce           =>   oSRAM_CE_N,
			io_sramCtrlPins_ramOut_noe           =>   oSRAM_OE_N,
			io_sramCtrlPins_ramOut_nwe           =>   oSRAM_WE_N,
			io_sramCtrlPins_ramOut_nlb           =>   oSRAM_LB_N,
			io_sramCtrlPins_ramOut_nub           =>   oSRAM_UB_N,

      io_sDHostControllerPins_clk          =>   SD_CLK,
			io_sDHostControllerPins_cmdIn        =>   SD_CMD,
			io_sDHostControllerPins_cmdOut       =>   sdcmd_out_dout,
			io_sDHostControllerPins_cmdToggle    =>   sdcmd_out_dout_ena,
			io_sDHostControllerPins_dataIn       =>   SD_DAT0,
			io_sDHostControllerPins_dataOut      =>   sddata_out_dout,
			io_sDHostControllerPins_dataToggle   =>   sddata_out_dout_ena
      );
end architecture rtl;
