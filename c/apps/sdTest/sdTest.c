#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <machine/spm.h>

volatile _SPM int *sd_ptr_1 = (volatile _SPM int *) 0xF00b0000;
volatile _SPM int *sd_ptr_2 = (volatile _SPM int *) 0xF00b0004;
volatile _SPM int *sd_resp_1 = (volatile _SPM int *) 0xF00b008;
volatile _SPM int *sd_status = (volatile _SPM int *) 0xF00b001c;
volatile _SPM int *sd_clk = (volatile _SPM int *) 0xF00b0020;

typedef union {
        /**
         * Struct representing the packet structure used in the thesis
         * useful for debugging.
         */
        struct {
                uint64_t en : 1;   /* end bit                 */
                uint64_t cr : 7;   /* CRC                     */
                uint64_t a3 : 8;   /* arg0                    */
                uint64_t a2 : 8;   /* arg1                    */
                uint64_t a1 : 8;   /* arg2                    */
                uint64_t a0 : 8;   /* arg3                    */
                uint64_t cm : 6;   /* Command for the SD card */
                uint64_t te : 1;   /* Terminating bit         */
                uint64_t st : 1;   /* Starting bit            */
                uint64_t ot : 16;  /* bits not used           */

        }e;

        /**
         * Struct that represent the command as the OCP protocol
         */
        struct {
                uint64_t l : 16;  /* First part of the command */
                uint64_t u : 32;  /* Second part of the command */
                uint64_t o : 16;  /* Other */

        }r;

        /**
         * Struct for managing the packet sent to the SD card.
         * Each command is composed by a payload of 40 bits
         * a CRC of 7 bits and an end bit.
         */
        struct {
                uint64_t e : 1;  /* End bit transmission, always 1 */
                uint64_t c : 7;  /* CRC_7 */
                uint64_t p : 40; /* Payload */
                uint64_t o : 16; /* Other */

        }c;

        /**
         * Usefull struct for accessing each byte separately
         */
        struct {
                uint64_t b5 : 8;
                uint64_t b4 : 8;
                uint64_t b3 : 8;
                uint64_t b2 : 8;
                uint64_t b1 : 8;
                uint64_t b0 : 8;
                uint64_t b6 : 8;
                uint64_t b7 : 8;
        }b;
        uint64_t cmd;
}SDCmd;

void sd_send(const SDCmd *dat)
{
        *sd_ptr_1 = dat->r.u;
        *sd_ptr_2 = dat->r.l;
}

void sd_cmd(SDCmd *dat)
{
        uint16_t i = 0;
        dat->e.st = 0x00;
        dat->e.te = 0x01;
        dat->e.en = 0x01;
        sd_send(dat);
}

int main()
{
        printf("Hello");
        SDCmd cmd = {0};
        *sd_clk = 100;
        while(1) {
                //sd_cmd(&cmd);
                //printf("The value of the status register is %d\n", *sd_status);

		cmd.e.cm = 8;
                cmd.e.a2 = 0x01;
                cmd.e.a3 = 0xaa;
                cmd.e.cr = 0x43;
		
                //cmd.e.cr = 0x4A;
                sd_cmd(&cmd);

                printf("The value of the status register is %d\n", *sd_status);
                printf("The value of the status register is %d\n", *sd_status);
                printf("The value of the status register is %d\n", *sd_status);
                printf("The value of the status register is %d\n", *sd_status);
                printf("The value of the respos1 register is %d\n", sd_resp_1[2]);
                //cmd.cmd = 0;
                //cmd.e.cm = 41;
                //cmd.e.a0 = 0x0;
                //cmd.e.a1 = 0x0;
                //cmd.e.a2 = 0x01;
                //cmd.e.a3 = 0xAA;
                //cmd.e.cr = 0x43;
                //sd_cmd(&cmd);
                //cmd.cmd = 0;
                //while(*sd_status < 2);
                //printf("The value of the status register is %d\n", *sd_status);
                //printf("The value of the status register is %d\n", *sd_status);
                //printf("The value of the status register is %d\n", *sd_status);
                //printf("The value of the respos1 register is %d\n", *sd_resp_1);
                //printf("The value of the status register is %d\n", *sd_status);
        }
        return 0;
}
